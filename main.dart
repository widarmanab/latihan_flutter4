import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Belajar Stateless Widget"),
        ),
        body: Center(
          child: Container(
            color: Colors.yellowAccent,
            width: 200,
            height: 50,
            child: Text(
              "Hallo saya sedang belajar membuat aplikasi menggunakan flutter",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.blue,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}
